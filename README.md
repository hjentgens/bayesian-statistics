# Bayesian Statistics

# Scope of the project
Goal of this project is an analysis of light curves from the [Kepler](https://science.nasa.gov/mission/kepler/) data using Bayesan statistical methods.

The data is taken from the [MAST Kepler Archive](https://archive.stsci.edu/pub/kepler/). 
This analysis focusses on [data](https://archive.stsci.edu/pub/kepler/lightcurves/0118/011853905/) from the confirmed planet Kepler-4b.

A full [manual](https://archive.stsci.edu/files/live/sites/mast/files/home/missions-and-data/k2/_documents/MAST_Kepler_Archive_Manual_2020.pdf) describing the data files is also available online.
There is also a simple [example](https://docs.astropy.org/en/stable/timeseries/index.html) using `astropy`on how to plot a light curve from the `.fits` files.
