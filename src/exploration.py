"""
This files addresses data exploration and model comparison tasks.
More specifically: 
- Fisher Matrix
- Bayes Ratio
- DIC

sadly exoplanet is incompatible with jax so we need to use autograd
for differentiation
"""

import numpy as np
import scipy


def fisher_matrix(model, MAP_values, t, cov_estimate, epsilon=None):
    """
    deliver a better estiamte for the covariance matrix
    using fisher information
    assumes gaussian likelihood and 
    covariance independent of the parameters
    returns the fisher matrix
    """
    def wrapper(params):
        period, T0, phi, F0, delta = params
        return model(period, T0, phi, F0, delta, t) 

    if epsilon is None:
        epsilon = np.sqrt(np.finfo(float).eps)

    jacobian = scipy.optimize.approx_fprime(MAP_values, wrapper, epsilon)
    inv_cov = np.linalg.inv(cov_estimate)
    #fisher_matrix = np.einsum("ip,ij,js", jacobian, inv_cov, jacobian)
    fisher_matrix = np.dot(jacobian.T, np.dot(inv_cov, jacobian))
    return fisher_matrix


def test_jacobian():
    def f(x):
        return np.array([x[0]**2, x[1]**2])
    x = np.array([1., 2.])
    epsilon = np.sqrt(np.finfo(float).eps)
    jac = scipy.optimize.approx_fprime(x, f, epsilon)
    print(jac)

if __name__ == "__main__":
    test_jacobian()
    print("done")