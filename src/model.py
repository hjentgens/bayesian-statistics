import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import scipy.special
import scipy.optimize
import scipy.integrate
import exoplanet as xo

G = 2942.94627 # R_sun^3/day^2/M_sun

def model(period, T0, phi, F0, delta, t):
    
    u = [0.5, 0.5] # limb darkening coefficients, u1 makes it deeper
    r_star = 1 # does not influence spectrum anymore 

    m_star = 4*period/(G*np.pi) * (r_star/T0)**3 #0.0156
    r = np.sqrt(delta)*r_star # radius of transiting body in same units as r_star -> influences deepness of dip
    

    # The light curve calculation requires an orbit
    orbit = xo.orbits.KeplerianOrbit(period=period, t0 = period*phi/(2*np.pi), r_star=r_star, m_star=m_star) # period/a, incl/b, ecc & omega, 

    light_curve = (
        xo.LimbDarkLightCurve(u)
        .get_light_curve(orbit=orbit, r=r, t=t)
        .eval()
    )

    return F0*(1 + light_curve[:,0])



