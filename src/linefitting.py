import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
import corner
import emcee
from analysis import model
from astropy.utils.data import get_pkg_data_filename
from astropy.timeseries import TimeSeries
import exoplanet as xo

##############################################
#import data
##############################################

#example_data = get_pkg_data_filename('../data/kplr010666592-2009131110544_slc.fits')
example_data = get_pkg_data_filename('timeseries/kplr010666592-2009131110544_slc.fits')
kepler = TimeSeries.read(example_data, format='kepler.fits', unit_parse_strict='silent')
#kepler = kepler[::5]

bnds = ((2.1, 2.3), (0.12, 0.18), (0, 2*np.pi), (1.025e2, 1.03e2), (0.003, 0.008))
#bnds = ((2.0, 2.4), (0.10, 0.2), (0, 2*np.pi), (1.02e6, 1.03e6), (0.001, 0.01))

flux = np.asarray(kepler['sap_flux'], dtype='float64')
flux = flux[~np.isnan(kepler['sap_flux'])]
flux /= 1e4

t = np.asarray(kepler.time.jd, dtype='float64')
t = t[~np.isnan(kepler['sap_flux'])]

sigma_flux = np.asarray(kepler['sap_flux_err'], dtype='float64')
sigma_flux = sigma_flux[~np.isnan(kepler['sap_flux'])]
sigma_flux /= 1e4

#print('prod ', np.prod(sigma_flux))

###############################################
#likelihood, prior, posterior
###############################################

def log_likelihood(P0, T0, phi, F0, delta, t, sigma_flux, flux):
    prediction = model(P0, T0, phi, F0, delta, t)

    sig_y = sigma_flux

    n = len(flux)
    return (
        -0.5*np.sum((flux - prediction)**2/sigma_flux**2)                    # Exponent
        - n/2*np.log(2*np.pi)                                                # Normalisation
    )

def log_prior(P0, T0, phi, F0, delta):
    mu_P = 2.0
    mu_T = 0.1
    mu_phi = np.pi
    mu_F = 1.0e2 #1.0e6
    mu_delta = 0.004
    #mu_incl = np.pi/2

    sigma_P = 0.5
    sigma_T = 0.05
    sigma_phi = np.pi/2
    sigma_F = 0.1e2 #0.1e6
    sigma_delta = 0.01
    #sigma_incl = np.pi/4

    return (
        -0.5*(P0-mu_P)**2/sigma_P**2          # P0 exponent
        -0.5*(T0-mu_T)**2/sigma_T**2          # T0 exponent
        -0.5*(phi-mu_phi)**2/sigma_phi**2       # phi exponent
        -0.5*(F0-mu_F)**2/sigma_F**2          # F0 exponent
        -0.5*(delta-mu_delta)**2/sigma_delta**2 # delta exponent
        #-0.5*(incl-mu_incl)**2/sigma_incl**2    #incl
        - 0.5*np.log(2*np.pi*sigma_P**2)       # Normalisation
        - 0.5*np.log(2*np.pi*sigma_T**2) 
        - 0.5*np.log(2*np.pi*sigma_phi**2)
        - 0.5*np.log(2*np.pi*sigma_F**2)
        - 0.5*np.log(2*np.pi*sigma_delta**2)
       # - 0.5*np.log(2*np.pi*sigma_incl**2)
    )

def log_posterior(P0, T0, phi, F0, delta, t, sigma_flux, flux):
    return log_likelihood(P0, T0, phi, F0, delta, t, sigma_flux, flux) + log_prior(P0, T0, phi, F0, delta)

def negative_log_posterior(theta, t, sigma_flux, flux):
    P0, T0, phi, F0, delta = theta
    return -log_posterior(P0, T0, phi, F0, delta, t, sigma_flux, flux)

###################################################
#initial values
###################################################

P0 = 2.2 # period in days
T0 = 0.15 # dip length in days
phi0 = 0.69*np.pi
F0 = 1.02e2#np.float64(1.027e6) # level
delta0 = 0.005 # relative dip  = (R_p/R_*)^2

########################################################
#optimization
########################################################

#MAP_result = scipy.optimize.minimize(fun=negative_log_posterior, x0=(P0,T0,phi0,F0,delta0), args=(t[::1000], sigma_flux[::1000], flux[::1000]), bounds = bnds)

#P0_MAP, T0_MAP, phi0_MAP, F0_MAP, delta0_MAP = MAP_result.x
""" print("MAP results")
print(f"P0_MAP = {P0_MAP:.3f}, T0_MAP = {T0_MAP:.3f}, phi0_MAP = {phi0_MAP:.3f}, F0_MAP = {F0_MAP:.3f}, delta0_MAP = {delta0_MAP:.3f}")

plt.errorbar(t, 1e4*flux, 1e4*sigma_flux, fmt=".", label="Data")
plt.plot(t, model(P0_MAP, T0_MAP, phi0_MAP, 1e4*F0_MAP, delta0_MAP, t), c="C1", label="MAP model")
plt.xlabel("$t$")
plt.ylabel("$flux$")
#plt.legend(frameon=False);
plt.show() """

# emcee passes an array of values for the sampled parameters
# This wrapper just splits the array theta into m and b
def log_posterior_wrapper(theta, t, sigma_flux, flux):
    P0, T0, phi, F0, delta = theta
    return log_posterior(P0, T0, phi, F0, delta, t, sigma_flux, flux)

# emcee requires some extra settings to run
n_param = 5       # Number of parameter we are sampling
n_walker = 11     # Number of walkers. This just needs to be 
                  # larger than 2*n_param + 1
n_step = 100     # How many steps each walker will take. The number
                  # of samples will be n_walker*n_step

print('delta0 ', delta0)

filename = "monitoring.h5"
backend = emcee.backends.HDFBackend(filename)
backend.reset(n_walker, n_param)

# The starting point for each walker
theta_init = np.array([P0,T0,phi0,F0,delta0]) + 0.1*np.random.normal(size=(n_walker, n_param))

sampler = emcee.EnsembleSampler(
    nwalkers=n_walker, ndim=n_param,
    log_prob_fn=log_posterior_wrapper,
    args=(t, sigma_flux, flux),
    backend=backend
)

print('finished initializing')

state = sampler.run_mcmc(theta_init, nsteps=n_step, progress=True)

print('check convergence')

max_n = 100

# track how the average autocorrelation time estimate changes
index = 0
autocorr = np.empty(max_n)
old_tau = np.inf

# sample for up to max_n steps
for sample in sampler.sample(theta_init, iterations=max_n, progress=True):
    print('entered loop')
    # check convergence every 100 steps
    if sampler.iteration % 100:
        continue
    
    print('checking...')
    # Compute the autocorrelation time so far
    tau = sampler.get_autocorr_time(tol=0)
    autocorr[index] = np.mean(tau)
    index += 1

    # Check convergence
    converged = np.all(tau * 100 < sampler.iteration)
    converged &= np.all(np.abs(old_tau - tau) / tau < 0.01)
    if converged:
        break
    print('not yet converged...')
    old_tau = tau

print('converged!')

n = 100 * np.arange(1, index + 1)
y = autocorr[:index]
plt.plot(n, n / 100.0, "--k")
plt.plot(n, y)
plt.xlim(0, n.max())
plt.ylim(0, y.max() + 0.1 * (y.max() - y.min()))
plt.xlabel("number of steps")
plt.ylabel(r"mean $\hat{\tau}$");

state = sampler.run_mcmc(theta_init, nsteps=n_step)

print('nearly there!')

#############################################################
#corner plot
############################################################+

# The samples will be correlated, this checks how correlated they are
# We will discuss this once we come to MCMC methods
print("Auto-correlation time:")
for name, value in zip(["P", "T", "phi", "F", "delta", "f"], sampler.get_autocorr_time()):
    print(f"{name} = {value:.1f}")

'''

print('one last bit')

# We need to discard the beginning of the chain (a few auto-correlation times)
# to get rid of the initial conditions
chain = sampler.get_chain(discard=300, thin=10, flat=True)

fig = plt.figure()
fig = corner.corner(
    chain,
    bins=40,
    labels=["P", "T", "phi", "F", "delta"],
    levels=1-np.exp(-0.5*np.array([1, 2])**2), # Credible contours corresponding
                                               # to 1 and 2 sigma in 2D
    quantiles=[0.025, 0.16, 0.84, 0.975],
    fig=fig
);
plt.show()
#plt.close()
'''