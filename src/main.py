import numpy as np
import matplotlib.pyplot as plt

from analysis import model


def main():
    print('Hello Other World!')
    x = np.linspace(-0.1, 0.1, 100)
    y = model(x, period=3.4, t0=0, r=0.1, alpha=0.3, f_star=100)
    plt.plot(x, y)
    plt.show()


if __name__ == '__main__':
    main()