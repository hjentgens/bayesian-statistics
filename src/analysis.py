import numpy as np
import exoplanet as xo
#from jax import jit

'''
def model(t, period, t0, alpha, r, f_star, incl=np.pi/2):
    
    orbit = xo.orbits.KeplerianOrbit(period=period, incl=incl, t0=t0)

    light_curve = (
        xo.LimbDarkLightCurve(u1=alpha, u2=alpha)
        .get_light_curve(orbit=orbit, r=r, t=t)
        .eval()
        )
    return light_curve + f_star
'''

# Model definitions
G = 2942.94627 # R_sun^3/day^2/M_sun

#@jit
def model(period, T0, phi, F0, delta, t):
    
    u = [0.5, 0.5] # limb darkening coefficients, u1 makes it deeper
    r_star = 1 # does not influence spectrum anymore 

    m_star = 4*period/(G*np.pi) * (r_star/T0)**3 #0.0156
    r = np.sqrt(np.abs(delta))*r_star # radius of transiting body in same units as r_star -> influences deepness of dip
    

    # The light curve calculation requires an orbit
    orbit = xo.orbits.KeplerianOrbit(period=period, t0 = period*phi/(2*np.pi), r_star=r_star, m_star=m_star) # period/a, incl/b, ecc & omega, 

    light_curve = (
        xo.LimbDarkLightCurve(u)
        .get_light_curve(orbit=orbit, r=r, t=t)
        .eval()
    )

    return F0*(1 + light_curve[:,0])


# We use the logarithm here for computational reasons
#@jit
def log_likelihood(period, T0, phi, F0, delta, t, sigma_y, y):
    prediction = model(period, T0, phi, F0, delta, t)

    sig_y = sigma_y # + f_true * prediction**2

    n = len(y)
    return (
        -0.5 * np.sum((y - prediction)**2/sig_y**2)  # Exponent
        - n/2*np.log(2*np.pi)
        - np.sum(np.log(sig_y))               # Normalisation
    )

def log_prior(period, T0, phi, F0, delta):

    '''
    return (
        #-0.5*(period-mu_P)**2/sigma_P**2      # m exponent
        #-0.5*(phi-mu_b)**2/sigma_b**2      # b exponent
        #- 0.5*np.log(2*np.pi*sigma_P**2) # Normalisation
        #- 0.5*np.log(2*np.pi*sigma_b**2) # Normalisation
        - np.log(3) # normalisation period
        - np.log(2*np.pi) # normalisation for phi
        - np.log(5e5) # normalisation F0
    )
    '''
    return 0.

#@jit
def log_posterior(period, T0, phi, F0, delta, t, sigma_y, y):
    return log_likelihood(period, T0, phi, F0, delta, t, sigma_y, y) + log_prior(period, T0, phi, F0, delta)

#@jit
# The scipy minimizer finds the minimum, so we need to take the 
def negative_log_posterior(theta, t, sigma_y, y):
    period, T0, phi, F0, delta = theta
    return -log_posterior(period, T0, phi, F0, delta, t, sigma_y, y)



def smoothListGaussian(myarray, degree=5):
    """
    Given a 1D array myarray, the code returns a Gaussian smoothed version of the array.
    """
    # Pad the array so that the final convolution uses the end values of myarray and returns an
    # array of the same size
    myarray = np.hstack([ [myarray[0]]*(degree-1),myarray,[myarray[-1]]*degree])
    window=degree*2-1  
    # Build the weights filter
    weight=np.array([1.0]*window)
    weightGauss=[]  
    for i in range(window):  
        i=i-degree+1  
        frac=i/float(window)  
        gauss=np.exp(-(4*frac)**2)
        weightGauss.append(gauss)  
    weight=np.array(weightGauss)*weight
    # create the smoothed array with a convolution with the window
    smoothed=np.array([0.0]*(len(myarray)-window))
    for i in range(len(smoothed)):  
        smoothed[i]=sum(myarray[i:i+window]*weight)/sum(weight)  
    return smoothed
